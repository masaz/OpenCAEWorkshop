set terminal pdf solid color lw 2 font "Helvetica,14"
set datafile separator ","
set style data linespoints
set xrange [0:*] 
set logscale xy
set key top left font "Helvetica,9"
set grid
set title '3-D Lid Driven cavity flow, Medium, 8M cells'

set xlabel "Number of nodes/VMs"
set xrange [0.8:80] 
set xtics (1,2,4,8,16,32,64)  font "Helvetica,11" 

set output "time.pdf"
set ylabel "Exectution time per step (<Lower is better)" 
set key bottom left font "Monospace,10" width 6
set yrange [0.1:100]
plot \
 "< grep -h ',032c,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:10 \
title "FOCUS VM32,DIC "\
,"< grep -h ',032c,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:10 \
title "FOCUS VM32,GAMG"\
,"< grep -h ',092c,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:10 \
title "FOCUS VM92,DIC "\
,"< grep -h ',092c,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:10 \
title "FOCUS VM92,GAMG"\
,"< grep -h 'wa.*,.*,FOAM-DIC-PCG.fixedNORM,' ../../Wisteria-No1/M/time.csv" using 3:10 \
title "Aquarius CPU,DIC "\
,"< grep -h 'wa.*,.*,FOAM-GAMG-PCG.fixedNORM,' ../../Wisteria-No1/M/time.csv" using 3:10 \
title "Aquarius CPU,GAMG"\
,100/x title "Linear" with l lt 1 lc 0 lw 0.5\
