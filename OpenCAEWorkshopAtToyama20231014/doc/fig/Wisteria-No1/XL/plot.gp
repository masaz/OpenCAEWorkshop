set terminal pdf solid color lw 2 font "Helvetica,14"
set datafile separator ","
set style data linespoints
set xrange [0:*] 
set logscale xy
#set key top left font "Helvetica,9"
set key bottom right font "Helvetica,10"
set grid

set xlabel "Number of nodes" offset 0,-0.5
set xrange [0.25:96] 
set xtics ("0.25\n(2GPU)" 0.25 ,"0.5\n(4GPU)" 0.5,"1\n(8GPU)" 1,"2\n(16GPU)"  2,"4\n(32GPU)"  4 ,"8\n(64GPU)"  8, 12, 24, 48, 96)  font "Helvetica,12" 

set output "time.pdf"
set ylabel "Exectution time per step (<Lower is better)" 
set key top right font "Monospace,10"
set yrange [1:1000]
plot \
 "< grep -h 'wo.*,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:10 \
title "A64FX,OpenFOAM,DIC "\
,"< grep -h 'wo.*,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:10 \
title "A64FX,OpenFOAM,GAMG"\
,"< grep -h 'wa.*,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:10 \
title "Xeon ,OpenFOAM,DIC "\
,"< grep -h 'wa.*,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:10 \
title "Xeon ,OpenFOAM,GAMG"\
,"< grep -h 'wa.*,.*,PETSc-ICC-CG.mpiaij.fixedNORM,' time.csv" using 3:10 \
title "Xeon ,PETSc   ,ICC "\
,"< grep -h 'wa.*,.*,PETSc-AMG-CG.mpiaij.fixedNORM,' time.csv" using 3:10 \
title "Xeon ,PETSc   ,AMG "\
,"< grep -h 'wa.*,.*,PETSc-AMG-CG.mpiaij.fixedNORM.caching,' time.csv" using 3:10 \
title "Xeon ,PETSc   ,AMGc"\
,"< grep -h 'wa.*,.*,PETSc-ICC-CG.aijcusparse.fixedNORM,' time.csv" using ($4/8):10 \
title "A100 ,PETSc   ,ICC "\
,"< grep -h 'wa.*,.*,RapidCFD-AINV-PCG.fixedNORM.gpuDirectTransfer_1,' time.csv" using ($4/8):10 \
title "A100 ,RapidCFD,AINV"\
,1000/(x/0.25) title "Linear" with l lt 1 lc 0 lw 0.5\

set output "token.pdf"
set yrange [*:*]
set ylabel "Token charge per step (<Lower is better)" 
set key top right font "Monospace,10"
plot \
 "< grep -h 'wo.*,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:($10*$3/3600) \
title "A64FX,OpenFOAM,DIC "\
,"< grep -h 'wo.*,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:($10*$3/3600) \
title "A64FX,OpenFOAM,GAMG"\
,"< grep -h 'wa.*,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:($10*$3*2/3600) \
title "Xeon ,OpenFOAM,DIC "\
,"< grep -h 'wa.*,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:($10*$3*2/3600) \
title "Xeon ,OpenFOAM,GAMG"\
,"< grep -h 'wa.*,.*,PETSc-ICC-CG.mpiaij.fixedNORM,' time.csv" using 3:($10*$3*2/3600) \
title "Xeon ,PETSc   ,ICC "\
,"< grep -h 'wa.*,.*,PETSc-AMG-CG.mpiaij.fixedNORM,' time.csv" using 3:($10*$3*2/3600) \
title "Xeon ,PETSc   ,AMG "\
,"< grep -h 'wa.*,.*,PETSc-AMG-CG.mpiaij.fixedNORM.caching,' time.csv" using 3:($10*$3*2/3600) \
title "Xeon ,PETSc   ,AMGc"\
,"< grep -h 'wa.*,.*,PETSc-ICC-CG.aijcusparse.fixedNORM,' time.csv" using ($4/8):($10*$11*3/3600) \
title "A100 ,PETSc   ,ICC "\
,"< grep -h 'wa.*,.*,RapidCFD-AINV-PCG.fixedNORM.gpuDirectTransfer_1,' time.csv" using ($4/8):($10*$11*3/3600) \
title "A100 ,RapidCFD,AINV"\

set output "gpu.pdf"
set xrange [0.25:8]
set yrange [*:*]
set ylabel "GPU utilization (Higher is better>)" 
unset logscale y
set key bottom left font "Monospace,10" width 6
plot \
 "< grep -h 'wa.*,.*,PETSc-ICC-CG.aijcusparse.fixedNORM,' time.csv" using ($4/8):13 \
title "A100 ,PETSc   ,ICC ,Max"\
,"< grep -h 'wa.*,.*,RapidCFD-AINV-PCG.fixedNORM.gpuDirectTransfer_1,' time.csv" using ($4/8):13 \
title "A100 ,RapidCFD,AINV,Max"\
,"< grep -h 'wa.*,.*,PETSc-ICC-CG.aijcusparse.fixedNORM,' time.csv" using ($4/8):12 \
title "A100 ,PETSc   ,ICC ,Ave"\
,"< grep -h 'wa.*,.*,RapidCFD-AINV-PCG.fixedNORM.gpuDirectTransfer_1,' time.csv" using ($4/8):12 \
title "A100 ,RapidCFD,AINV,Ave"\
