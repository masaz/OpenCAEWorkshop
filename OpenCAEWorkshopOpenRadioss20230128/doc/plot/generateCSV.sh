#!/bin/sh
(
echo "#proc,thread,core,node,time(CONTACT_SORTING),time(CONTACT_FORCES),time(INCLUDING_CONTACT_NORMALS),time(ELEMENT_FORCES),time(KINEMATIC_COND),time(INTEGRATION),time(ASSEMBLING),time(OTHERS),time(TOTAL),time(ELAPSED),mem(TOTAL),mem(MAXIMUM),mem(MINIMUM),mem(AVERAGE),disk(TOTAL),disk(ANIMATION),disk(RESTART),CYCLES,OS,bit,compiler,mpi,ID,date"
for dir in np_*/nt_*
do
    file=$dir/log.engine_linux64_gf_0001
    [ ! -f $file ] && continue
    proc=${dir%%/*}
    proc=${proc#np_}
    thread=${dir##*/}
    thread=${thread#nt_}
    node=$(echo "($proc*$thread+56-1)/56" | bc)
    core=$(echo "$proc*$thread" | bc)
    awk -v core=$core -v node=$node -v proc=$proc -v thread=$thread \
'\
/compiler/ {OS=$2;bit=$3;compiler=$5;mpi=$7;if (mpi=="**") {mpi=""}} \
/CommitID/ {ID=$3} \
/[0-9][0-9]\/[0-9][0-9]\// {date=$1} \
/^ CONTACT SORTING/ {timeCS=$3} \
/^ CONTACT SORTING/ {timeCS=$3} \
/^ CONTACT FORCES/ {timeCF=$3} \
/^ \.\.INCLUDING CONTACT NORMALS/ {timeICN=$4} \
/^ ELEMENT FORCES/ {timeEF=$3} \
/^ KINEMATIC COND/ {timeKC=$3} \
/^ INTEGRATION/ {timeI=$2} \
/^ ASSEMBLING/ {timeA=$2} \
/^ OTHERS / {timeO=$4;if (timeO=="*********") {timeO=""}} \
/^ TOTAL\./ {timeT=$2} \
/^ TOTAL MEMORY USED/ {memT=$5} \
/^ MAXIMUM MEMORY PER PROCESSOR/ {memMAX=$5} \
/^ MINIMUM MEMORY PER PROCESSOR/ {memMIN=$5} \
/^ AVERAGE MEMORY PER PROCESSOR/ {memAVE=$5} \
/^ TOTAL DISK SPACE USED/ {diskT=$6} \
/^ ANIMATION/ {diskA=$4} \
/^ RESTART FILE SIZE/ {diskR=$5} \
/ELAPSED TIME     =/ {timeAll=$4} \
/TOTAL NUMBER OF CYCLES/ {cycles=$6} \
/NORMAL TERMINATION/ {end=1} \
END {printf "%d,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",proc,thread,core,node,timeCS,timeCF,timeICN,timeEF,timeKC,timeI,timeA,timeO,timeT,timeAll,memT,memMAX,memMIN,memAVE,diskT,diskA,diskR,cycles,OS,bit,compiler,mpi,ID,date}' $file
done

) | sort -t ',' -k 1,1n -k 2,2n -k 3,3n -k 4,4n > OpenRadioss_benchmarkTest.csv

