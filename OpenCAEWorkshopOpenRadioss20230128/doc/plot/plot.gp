set terminal pdf font "Arial,14" lw 2
set colorsequence podo
set style data lp
set key above box font "Arial,12"
set grid mxtics mytics xtics ytics

set ylabel "Speed (#cycles per second)"
set logscale
set yrange [0.1:1000]

set xlabel "#Threads"

set xrange [1:56]
set xtics ('1' 1, '2' 2, '4' 4, '8' 8, '14' 14, '28' 28, '56' 56)

set output "thread-speed-each-mpi.pdf"
plot \
 "< awk -F ',' '$1==1 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "no MPI" \
,"< awk -F ',' '$1==2 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "2 MPI" \
,"< awk -F ',' '$1==4 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "4 MPI" \
,"< awk -F ',' '$1==7 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "7 MPI" \
,"< awk -F ',' '$1==8 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "8 MPI" \
,"< awk -F ',' '$1==14 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "14 MPI" \
,"< awk -F ',' '$1==16 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "16 MPI" \
,"< awk -F ',' '$1==28 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "28 MPI" \
,"< awk -F ',' '$1==32 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "32 MPI" \
,"< awk -F ',' '$1==56 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "56 MPI" \
,"< awk -F ',' '$1==64 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "64 MPI" \
,"< awk -F ',' '$1==128 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "128 MPI" \
,"< awk -F ',' '$1==112 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "112 MPI" \
,"< awk -F ',' '$1==224 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "224 MPI" \
,"< awk -F ',' '$1==448 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "448 MPI" \
,"< awk -F ',' '$1==512 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "512 MPI" \
,"< awk -F ',' '$1==896 {print $2,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "896 MPI" \
,0.1*x title 'Linear' with l lw 1\

set xlabel "#Cores"

set xrange [1:896]
set xtics ('1' 1, '2' 2, '4' 4, '8' 8, '14' 14, '28' 28, '56' 56, '112' 112, '224' 224, '448' 448, '896' 896)

set output "core-speed-each-thread.pdf"
plot \
 "< awk -F ',' '$2==1 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "1 Thread"\
,"< awk -F ',' '$2==2 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "2 Thread"\
,"< awk -F ',' '$2==4 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "4 Thread"\
,"< awk -F ',' '$2==7 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "7 Thread"\
,"< awk -F ',' '$2==8 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "8 Thread"\
,"< awk -F ',' '$2==14 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "14 Thread"\
,"< awk -F ',' '$2==28 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "28 Thread"\
,"< awk -F ',' '$2==56 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "56 Thread"\
,0.1*x title 'Linear' with l lw 1\

set output "core-speed-each-mpi.pdf"
plot \
 "< awk -F ',' '$1==1 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "no MPI" \
,"< awk -F ',' '$1==2 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "2 MPI" \
,"< awk -F ',' '$1==4 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "4 MPI" \
,"< awk -F ',' '$1==7 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "7 MPI" \
,"< awk -F ',' '$1==8 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "8 MPI" \
,"< awk -F ',' '$1==14 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "14 MPI" \
,"< awk -F ',' '$1==16 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "16 MPI" \
,"< awk -F ',' '$1==28 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "28 MPI" \
,"< awk -F ',' '$1==32 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "32 MPI" \
,"< awk -F ',' '$1==56 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "56 MPI" \
,"< awk -F ',' '$1==64 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "64 MPI" \
,"< awk -F ',' '$1==128 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "128 MPI" \
,"< awk -F ',' '$1==112 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "112 MPI" \
,"< awk -F ',' '$1==224 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "224 MPI" \
,"< awk -F ',' '$1==448 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "448 MPI" \
,"< awk -F ',' '$1==512 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "512 MPI" \
,"< awk -F ',' '$1==896 {print $3,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "896 MPI" \
,0.1*x title 'Linear' with l lw 1\

set xlabel "#Nodes"

set xrange [1:16]
set xtics ('1' 1, '2' 2, '4' 4, '8' 8, '16' 16)

set output "node-speed-each-thread.pdf"
plot \
 "< awk -F ',' '$2==1 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "1 Thread"\
,"< awk -F ',' '$2==2 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "2 Thread"\
,"< awk -F ',' '$2==4 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "4 Thread"\
,"< awk -F ',' '$2==7 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "7 Thread"\
,"< awk -F ',' '$2==8 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "8 Thread"\
,"< awk -F ',' '$2==14 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "14 Thread"\
,"< awk -F ',' '$2==28 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "28 Thread"\
,"< awk -F ',' '$2==56 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "56 Thread"\
,0.1*x title 'Linear' with l lw 1\

set output "node-speed-each-mpi.pdf"
plot \
 "< awk -F ',' '$1==1 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "no MPI" \
,"< awk -F ',' '$1==2 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "2 MPI" \
,"< awk -F ',' '$1==4 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "4 MPI" \
,"< awk -F ',' '$1==7 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "7 MPI" \
,"< awk -F ',' '$1==8 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "8 MPI" \
,"< awk -F ',' '$1==14 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "14 MPI" \
,"< awk -F ',' '$1==16 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "16 MPI" \
,"< awk -F ',' '$1==28 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "28 MPI" \
,"< awk -F ',' '$1==32 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "32 MPI" \
,"< awk -F ',' '$1==56 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "56 MPI" \
,"< awk -F ',' '$1==64 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "64 MPI" \
,"< awk -F ',' '$1==128 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "128 MPI" \
,"< awk -F ',' '$1==112 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "112 MPI" \
,"< awk -F ',' '$1==224 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "224 MPI" \
,"< awk -F ',' '$1==448 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "448 MPI" \
,"< awk -F ',' '$1==512 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "512 MPI" \
,"< awk -F ',' '$1==896 {print $4,$22*$3/$13}' OpenRadioss_benchmarkTest.csv" title "896 MPI" \
,0.1*x title 'linear' with l lw 1\

set ylabel "Parallel Efficiency [%]"
set output "parallelEfficiency.pdf"
unset key
unset logscale y
set yrange [0:160]
plot \
"< awk -F ',' '$1==2 && $2==28 && $4==1 {t0=$13/$3;print $4,100}  $2==56 && $4>1 {print $4,t0/$13*$3/$4*100}' OpenRadioss_benchmarkTest.csv"\
,100 title "Linear" with line lt -1 lw 0.5

