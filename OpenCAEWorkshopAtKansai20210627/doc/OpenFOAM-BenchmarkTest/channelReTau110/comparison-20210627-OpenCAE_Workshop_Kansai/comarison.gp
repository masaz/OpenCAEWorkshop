set terminal pdf solid color lw 2 font "Helvetica,14"
set datafile separator ","
set style data linespoints
set xlabel "Number of GPUs"
set xrange [0:*] 
set logscale xy
set key bottom right
set grid

set xlabel "Number of GPUs"
set xrange [1:8] 
set xtics (1, 2, 4 , 8)
set ylabel "Time steps per hour (Higher is better>)" 

set output "mesh_0.37M-single_node.pdf"
plot \
1000*x title "Linear"\
,"< grep -h ',120,65,48,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_0.37M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Type II, No GPU direct"\
,"< grep -h ',120,65,48,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_0.37M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Type II, GPU direct"\
,"< grep -h ',120,65,48,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-*-RapidCFD-mesh_0.37M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_0.37M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Aquarius, No GPU direct"\
,"< grep -h ',120,65,48,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-*-RapidCFD-mesh_0.37M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Aquarius, GPU direct"\

set output "mesh_3M-single_node.pdf"
plot \
1000*x title "Linear"\
,"< grep -h ',240,130,96,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_3M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Type II, No GPU direct"\
,"< grep -h ',240,130,96,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_3M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Type II, GPU direct"\
,"< grep -h ',240,130,96,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_3M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_3M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Aquarius, No GPU direct"\
,"< grep -h ',240,130,96,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_3M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Aquarius, GPU direct"\

set output "mesh_24M-single_node.pdf"
plot \
100*x title "Linear"\
,"< grep -h ',480,260,192,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_24M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Type II, No GPU direct"\
,"< grep -h ',480,260,192,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,2,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_24M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Type II, GPU direct"\
,"< grep -h ',480,260,192,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_24M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_24M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Aquarius, No GPU direct"\
,"< grep -h ',480,260,192,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_24M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Aquarius, GPU direct"\


set output "mesh_48M-single_node.pdf"
plot \
100*x title "Linear"\
,"< grep -h ',605,328,242,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_48M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Type II, No GPU direct"\
,"< grep -h ',605,328,242,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,2,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_48M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Type II, GPU direct"\
,"< grep -h ',605,328,242,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_48M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_48M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Aquarius, No GPU direct"\
,"< grep -h ',605,328,242,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_48M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Aquarius, GPU direct"\


set output "mesh_96M-single_node.pdf"
plot \
10*x title "Linear"\
,"< grep -h ',762,412,306,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_96M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Aquarius, No GPU direct"\
,"< grep -h ',762,412,306,1,.*,Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_96M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Aquarius, GPU direct"\

#"< grep -h ',762,412,306,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6):(3600/$1) title "Type II, No GPU direct"\
#,"< grep -h ',762,412,306,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,2,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_96M-gpuDirectTransfer_1.json.csv" using ($5*$6):(3600/$1) title "Type II, GPU direct"\

set xlabel "Number of nodes"
set xrange [0.124:49] 
set xtics ("1/8" 0.125, "1/4" 0.25, "1/2" 0.5, 1, 2, 4, 8, 12, 24, 48)

set output "mesh_0.37M.pdf"
plot \
1000*x title "Linear"\
,"< grep -h ',Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_0.37M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6/8.0):(3600/$1) title "Aquarius(RapidCFD)"\
,"< grep -h ',120,65,48,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_0.37M-gpuDirectTransfer_0.json.csv;grep -h ',Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-ppn_4-mesh_0.37M-gpuDirectTransfer_0.json.csv" using ($5*$6/4.0):(3600/$1) title "Type II(RapidCFD)"\
,"< grep ',48,1,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_0.37M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_0.37M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Odyssey(OpenFOAM)"\
,"< grep ',48,1,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_0.37M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_0.37M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Type I(OpenFOAM)"\
,"< grep ',Gcc10_2_0,DP,32,Opt,INTELMPI2019_7_217,0.05,PCG,DIC,PCG,DIC,' ../Oakbridge-CX-No1/tuneOpenFOAM-mesh_0.37M.json.csv" using 5:(3600/$1) title "Oakbridge-CX(OpenFOAM)"\

set output "mesh_3M.pdf"
plot \
100*x title "Linear"\
,"< grep -h ',Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_3M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6/8.0):(3600/$1) title "Aquarius(RapidCFD)"\
,"< grep -h ',240,130,96,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_3M-gpuDirectTransfer_0.json.csv;grep -h ',Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-ppn_4-mesh_3M-gpuDirectTransfer_0.json.csv" using ($5*$6/4.0):(3600/$1) title "Type II(RapidCFD)"\
,"< grep ',48,1,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_3M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_3M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Odyssey(OpenFOAM)"\
,"< grep ',48,1,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_3M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_3M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Type I(OpenFOAM)"\
,"< grep ',Gcc10_2_0,DP,32,Opt,INTELMPI2019_7_217,0.05,PCG,DIC,PCG,DIC,' ../Oakbridge-CX-No1/tuneOpenFOAM-mesh_3M.json.csv" using 5:(3600/$1) title "Oakbridge-CX(OpenFOAM)"\

set output "mesh_24M.pdf"
plot \
100*x title "Linear"\
,"< grep -h ',Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_24M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6/8.0):(3600/$1) title "Aquarius(RapidCFD)"\
,"< grep -h ',480,260,192,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_24M-gpuDirectTransfer_0.json.csv;grep -h ',Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-ppn_4-mesh_24M-gpuDirectTransfer_0.json.csv" using ($5*$6/4.0):(3600/$1) title "Type II(RapidCFD)"\
,"< grep ',48,1,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_24M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_24M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Odyssey(OpenFOAM)"\
,"< grep ',48,1,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_24M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_24M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Type I(OpenFOAM)"\
,"< grep ',Gcc10_2_0,DP,32,Opt,INTELMPI2019_7_217,0.05,PCG,DIC,PCG,DIC,' ../Oakbridge-CX-No1/tuneOpenFOAM-mesh_24M.json.csv" using 5:(3600/$1) title "Oakbridge-CX(OpenFOAM)"\

set output "mesh_48M.pdf"
plot \
100*x title "Linear"\
,"< grep -h ',Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_48M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6/8.0):(3600/$1) title "Aquarius(RapidCFD)"\
,"< grep -h ',605,328,242,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_48M-gpuDirectTransfer_0.json.csv;grep -h ',Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-ppn_4-mesh_48M-gpuDirectTransfer_0.json.csv" using ($5*$6/4.0):(3600/$1) title "Type II(RapidCFD)"\
,"< grep ',48,1,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_48M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_48M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Odyssey(OpenFOAM)"\
,"< grep ',48,1,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_48M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_48M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Type I(OpenFOAM)"\

set output "mesh_96M.pdf"
plot \
100*x title "Linear"\
,"< grep -h ',Nvcc11.2Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Wisteria-No1/tuneOpenFOAM-share-RapidCFD-mesh_96M-gpuDirectTransfer_0.json.csv ../Wisteria-No1/tuneOpenFOAM-Aquarius-RapidCFD-ppn_8-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6/8.0):(3600/$1) title "Aquarius(RapidCFD)"\
,"< grep -h ',762,412,306,1,.*,Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-*-mesh_96M-gpuDirectTransfer_0.json.csv;grep -h ',Nvcc11.2.1Gcc4.8.5,DP,.*,SYSTEMOPENMPI,.*,compact,' ../Flow-No4/tuneOpenFOAM-TypeII-ppn_4-mesh_96M-gpuDirectTransfer_0.json.csv" using ($5*$6/4.0):(3600/$1) title "Type II(RapidCFD)"\
,"< grep ',48,1,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_96M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Wisteria-No1/tuneOpenFOAM-Odyssey-module_v1812-mesh_96M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Odyssey(OpenFOAM)"\
,"< grep ',48,1,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_96M.json.csv;grep ',[^x]*:mesh,.*,DP,' ../Flow-No4/tuneOpenFOAM-TypeI-module_v1812-tune-mesh_96M.json.csv | sed s/:mesh//" using 5:(3600/$1) title "Type I(OpenFOAM)"\

